﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_GridView_Task_Select_Automatic.Models
{
    public class PersonEntity
    {
        public decimal? BusinessEntityID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}