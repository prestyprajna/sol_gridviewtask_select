﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sol_GridView_Task_Select_Automatic.DAL;
using Sol_GridView_Task_Select_Automatic.Models;
using Newtonsoft.Json;

namespace Sol_GridView_Task_Select_Automatic.UI.GridViewDisplay_Custom_select
{
    public partial class GridView_CustomSelect : System.Web.UI.Page
    {
        #region  declaration
        private PersonDal personDalObj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void gvPersonsData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            await Task.Run(() =>
            {

                if (e.CommandName == "Select")
                {
                    var rowIndex = Convert.ToInt32(e.CommandArgument);

                    var personEntityObj = new PersonEntity()
                    {
                        BusinessEntityID = Convert.ToInt32(gvPersonsData.DataKeys[rowIndex].Value),
                        FirstName = ((Label)gvPersonsData.Rows[rowIndex].Cells[1].FindControl("lblFirstName")).Text,
                        LastName = ((Label)gvPersonsData.Rows[rowIndex].Cells[1].FindControl("lblLastName")).Text
                    };

                    var jsonPersonData = JsonConvert.SerializeObject(personEntityObj);

                    Session["PersonData"] = jsonPersonData;

                    Response.Redirect("~/UI/UpdatePage/UpdatePage.aspx", false);

                }


            });
            
        }

        #region  public methods

        public async Task<SelectResult> BindPersonData(int startRowIndex, int maximumRows)
        {
            return await Task.Run(async () =>
            {
                personDalObj = new PersonDal();

                var getPersonCountData = Convert.ToInt32(await personDalObj?.GetPersonDataCount());

                var getPersonDataPagination = await personDalObj?.PersonDataPagination(startRowIndex, maximumRows);

                return new SelectResult(getPersonCountData, getPersonDataPagination);

            });
        }

        #endregion

    }
}