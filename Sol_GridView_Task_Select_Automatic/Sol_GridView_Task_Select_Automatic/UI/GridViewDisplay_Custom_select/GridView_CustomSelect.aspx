﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="GridView_CustomSelect.aspx.cs" Inherits="Sol_GridView_Task_Select_Automatic.UI.GridViewDisplay_Custom_select.GridView_CustomSelect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvPersonsData" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" DataKeyNames="BusinessEntityID" GridLines="None" OnRowCommand="gvPersonsData_RowCommand" SelectMethod="BindPersonData" ItemType="Sol_GridView_Task_Select_Automatic.Models.PersonEntity">
                   
                     <Columns>

                         <asp:TemplateField HeaderText="Select">
                             <ItemTemplate>
                                 <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" CommandArgument="<%# ((GridViewRow)Container).RowIndex%>" />
                             </ItemTemplate>
                         </asp:TemplateField>

                         <asp:TemplateField HeaderText="FirstName">
                             <ItemTemplate> 
                                 <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>

                         <asp:TemplateField HeaderText="LastName">
                             <ItemTemplate> 
                                 <asp:Label ID="lblLastName" runat="server" Text='<%#Eval("LastName") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                        
                    </Columns>

                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
