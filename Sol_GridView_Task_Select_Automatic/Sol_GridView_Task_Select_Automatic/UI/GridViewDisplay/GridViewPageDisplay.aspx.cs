﻿using Sol_GridView_Task_Select_Automatic.DAL;
using Sol_GridView_Task_Select_Automatic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_GridView_Task_Select_Automatic.UI.GridViewDisplay
{
    public partial class GridViewPageDisplay : System.Web.UI.Page
    {
        #region  declaration
        private PersonDal personDalObj = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region  public methods

        public async Task<SelectResult> BindPersonData(int startRowIndex, int maximumRows)
        {
            return await Task.Run(async() =>
            {
                personDalObj = new PersonDal();

                var getPersonCountData = Convert.ToInt32(await personDalObj?.GetPersonDataCount());

                var getPersonDataPagination = await personDalObj?.PersonDataPagination(startRowIndex, maximumRows);

                return new SelectResult(getPersonCountData, getPersonDataPagination);

            });
        }

        #endregion

        protected async void gvPerson_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            await this.MapDataFromUiToEntity(sender, e);
        }
    }

}