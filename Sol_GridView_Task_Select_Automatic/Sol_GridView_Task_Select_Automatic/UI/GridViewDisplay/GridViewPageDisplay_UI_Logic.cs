﻿using Newtonsoft.Json;
using Sol_GridView_Task_Select_Automatic.DAL.ORD;
using Sol_GridView_Task_Select_Automatic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_GridView_Task_Select_Automatic.UI.GridViewDisplay
{
    public partial class GridViewPageDisplay
    {

        public async Task MapDataFromUiToEntity(object sender, GridViewSelectEventArgs e)
        {
            await Task.Run(() =>
            {
                var personEntityObj = new PersonEntity()
                {
                    BusinessEntityID = Convert.ToDecimal(gvPerson.DataKeys[e.NewSelectedIndex].Value),
                    FirstName = gvPerson.Rows[e.NewSelectedIndex].Cells[2].Text,
                    LastName=gvPerson.Rows[e.NewSelectedIndex].Cells[3].Text
                };

                var jsonPersonData=JsonConvert.SerializeObject(personEntityObj);

                Session["PersonData"] = jsonPersonData;

                Response.Redirect("~/UI/UpdatePage/UpdatePage.aspx", false);


            });
        }
    }
}