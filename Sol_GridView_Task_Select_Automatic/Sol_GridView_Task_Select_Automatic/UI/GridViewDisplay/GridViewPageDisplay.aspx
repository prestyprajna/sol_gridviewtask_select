﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="GridViewPageDisplay.aspx.cs" Inherits="Sol_GridView_Task_Select_Automatic.UI.GridViewDisplay.GridViewPageDisplay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

          <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel id="updatePanel" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvPerson" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="BusinessEntityID" ForeColor="Black" GridLines="Vertical" SelectMethod="BindPersonData" AutoGenerateSelectButton="true" OnSelectedIndexChanging="gvPerson_SelectedIndexChanging" ItemType="Sol_GridView_Task_Select_Automatic.Models.PersonEntity">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="BusinessEntityID" HeaderText="BusinessEntityID" Visible="False" />
                        <asp:BoundField DataField="FirstName" HeaderText="FirstName" />
                        <asp:BoundField DataField="LastName" HeaderText="LastName" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                    <SortedAscendingHeaderStyle BackColor="#848384" />
                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                    <SortedDescendingHeaderStyle BackColor="#575357" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
