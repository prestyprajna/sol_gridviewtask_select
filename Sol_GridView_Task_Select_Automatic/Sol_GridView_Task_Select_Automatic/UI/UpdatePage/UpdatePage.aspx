﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="UpdatePage.aspx.cs" Inherits="Sol_GridView_Task_Select_Automatic.UI.UpdatePage.UpdatePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>

                <table>
                    
                    <tr>
                        <td>
                            <asp:TextBox  ID="txtFirstName" runat="server" placeholder="FirstName"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:TextBox  ID="txtLastName" runat="server" placeholder="LastName"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" />
                        </td>
                    </tr>

                </table>
                
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
