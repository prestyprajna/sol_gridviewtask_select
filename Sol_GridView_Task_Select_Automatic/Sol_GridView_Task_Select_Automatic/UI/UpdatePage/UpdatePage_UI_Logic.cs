﻿using Newtonsoft.Json;
using Sol_GridView_Task_Select_Automatic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_GridView_Task_Select_Automatic.UI.UpdatePage
{
    public partial class UpdatePage
    {
        public async Task BindPersonData()
        {
             await Task.Run(async () =>
            {
                PersonEntity personEntityObj = JsonConvert.DeserializeObject<PersonEntity>((Session["PersonData"] as String));

                await this.DataMapping(personEntityObj);
            });
        }

        public async Task DataMapping(PersonEntity personEntityObj)
        {
            await Task.Run(() =>
            {
                this.PersonIdBinding = personEntityObj?.BusinessEntityID;
                this.FirstNameBinding = personEntityObj?.FirstName;
                this.LastNameBinding = personEntityObj?.LastName;
            });
        }
    }
}