﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_GridView_Task_Select_Automatic.UI.UpdatePage
{
    public partial class UpdatePage
    {
        #region

        public  string FirstNameBinding
        {
            get
            {
                return txtFirstName.Text;
            }
            set
            {
                txtFirstName.Text = value;
            }
        }

        public string LastNameBinding
        {
            get
            {
                return txtLastName.Text;
            }
            set
            {
                txtLastName.Text = value;
            }
        }

        public decimal? PersonIdBinding
        {
            get
            {
                return Convert.ToDecimal(ViewState["PersonID"]);
            }
            set
            {
                ViewState["PersonID"] = value;
            }
        }
        #endregion
    }
}