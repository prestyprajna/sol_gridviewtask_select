﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_GridView_Task_Select_Automatic.UI.UpdatePage
{
    public partial class UpdatePage : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
                await this.BindPersonData();
            }
        }
    }
}