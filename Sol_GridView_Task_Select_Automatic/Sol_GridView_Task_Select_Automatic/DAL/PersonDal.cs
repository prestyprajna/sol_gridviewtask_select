﻿using Sol_GridView_Task_Select_Automatic.DAL.ORD;
using Sol_GridView_Task_Select_Automatic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_GridView_Task_Select_Automatic.DAL
{
    public class PersonDal
    {
        #region  declaration
        private PersonDCDataContext _dc = null;
        #endregion

        #region  constructor
        public PersonDal()
        {
            _dc = new PersonDCDataContext();
        }

        #endregion

        #region  public methods

        public async Task<List<PersonEntity>> GetPersonData()
        {
            return await Task.Run(() =>
            {
                var getPersonData = _dc
                ?.Persons
                ?.AsEnumerable()
                ?.Select((lePersonObj) => new PersonEntity()
                {
                    BusinessEntityID = lePersonObj?.BusinessEntityID,
                    FirstName = lePersonObj?.FirstName,
                    LastName = lePersonObj?.LastName
                })
                ?.ToList();

                return getPersonData;
            });
        }

        public async Task<int?> GetPersonDataCount()
        {
            return await Task.Run(async () =>
            {
                var getCount = (await this.GetPersonData())
                                ?.AsEnumerable()
                                ?.Count();

                return getCount;
            });
        }

        public async Task<List<PersonEntity>> PersonDataPagination(int startRowIndex, int maximumRows)
        {
            return await Task.Run(async() =>
            {
                return (await this.GetPersonData())
                        ?.AsEnumerable()
                        ?.Skip(startRowIndex)
                        ?.Take(maximumRows)
                        ?.ToList();
            });
        }
             
        #endregion
    }
}